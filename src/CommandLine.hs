{-#LANGUAGE OverloadedStrings#-}

module CommandLine
    ( 
      ioCommand
    ) where

import System.Environment (getArgs)
import System.Directory (getDirectoryContents)
import Data.Maybe
import Control.Applicative (liftA3)
import qualified Data.Map as M
import qualified Data.List as L
import qualified Data.ByteString.Char8 as B

import Fastq2Count (fq2ctMultiInput)

ioCommand :: IO()
ioCommand = do
  args <- getArgs
  case checkArgs args of
     False -> usage
     _     -> runProg args

checkArgs xs = if (length xs) /= 2 then False else True

usage = do
  putStrLn "Usage: crisprScreen <input fastq file directory> <reference file>"
  putStrLn "\nCount reads for individual gRNAs. By default, a read count matrix will be printed out to stdout.\n"

filterFq = filter isFastq 
  where pathSuffix path = reverse $ takeWhile (/= '.') $ reverse path
        isFastq path = let sf = pathSuffix path in
                           or [ sf == "fastq"
                              , sf == "fq"
                              , sf == "Fastq"]

-- tail get rid of '.'
rmSuffix path = reverse $ tail $ dropWhile (/= '.') $ reverse path

-- turn results from fq2tMutiInput to B.ByteString
-- (M.Map B.ByteString [Int] -> B.ByteString
showRes m = B.unlines eachLine
   where mapList = M.toList m
         grnaNames = map fst mapList
         geneNames = map getGeneName grnaNames
         getGeneName =  B.reverse . B.tail . snd . B.break (== '-') . B.reverse      
         -- take care genes like "11-Sep-1"
         countNumber = map (B.intercalate "\t" . map showInt2Bs . snd) mapList
         eachLine = map (\(x, y, z) -> B.intercalate "\t" [x, y, z]) $ 
                    L.zip3 grnaNames geneNames countNumber

showInt2Bs::Int -> B.ByteString
showInt2Bs i = B.pack $ show i

-- run fq2ctMultiInput <ref input> <fq inputs>
runProg args = do
  let fqDir = (args!!0)
  let refInput = (args!!1)
  fqInputs <- filterFq <$> getDirectoryContents fqDir
  let fullFqInputs = map (\x -> fqDir ++ "/" ++ x) fqInputs 
  let fqNames = map rmSuffix fqInputs
  output <- showRes <$> fq2ctMultiInput refInput fullFqInputs
  let header = B.intercalate "\t" $ map B.pack $ ["sgRNA", "Gene"] ++ fqNames
  B.putStrLn header
  B.putStrLn output

