module Util
    (  
      groupCount
    )
where

import qualified Data.List as L
import Data.Ord (comparing)

-- utility functions



groupCount x = zip (map head groupedOnes) (map length groupedOnes)
  where groupedOnes = groupDes x
        
groupPct l = zip (map head groupedOnes) (map pct groupedOnes) 
  where groupedOnes = groupDes l
        totalLength = L.genericLength l
        pct x = L.genericLength x / totalLength

groupDes x = L.reverse . L.sortBy (comparing length) . L.group . L.reverse . L.sort $ x
