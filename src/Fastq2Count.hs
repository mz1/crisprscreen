{-#LANGUAGE OverloadedStrings#-}

{- VirusLib.hs
   Analyze gRNA virus library, to see the sequences of gRNAs

   Min Zhang
   Martin Lab
   8-8-2014
   5-4-2015 revise for SAM library (human)
   6-14-2016 revision v0.2

-------------LOF-----------------------------------
   The sequence of the construct:
   ACGAAACACC   G GGATTTGCTGATGTCAGCT  GTTTTAGAGCT
   U6 prom      +1 gRNA 20 bp         gRNA scaffold
  
   for each sequence, find U6 promoter, take 20bp
------------LOF------------------------------------

------------GOF------------------------------------
gctttatatatcttGTGGAAAGG ACGAAACACC G NNNNNNNNNNNNNNNNNNN (19 bp) gttttagagct aggccAACATGAGGATCACCCATGTCTGCAGggcctagcaagttaaaataaggctagtccgttatcaacttggc

actually the sequence is exactly like the LOF library, don't need to change code at all. make the library gRNA list.
------------GOF------------------------------------
-}

module Fastq2Count
  (fq2ctMultiInput)
where

import qualified Data.ByteString.Char8 as B
import qualified Data.ByteString as Bs (findSubstring, isInfixOf)
import Control.Applicative
import Control.Monad (mapM)
import qualified Data.Map as M
import qualified Data.List as L
import qualified Data.Maybe as Maybe
import Util

fq2ctMultiInput:: Traversable t =>
     FilePath -> t FilePath -> IO (M.Map B.ByteString [Int])
fq2ctMultiInput refinput fqinputs = do
  ref <- M.fromList . getLib <$> inputIO refinput
  let gNameZeroMap = M.fromList $ zip (M.elems ref) (repeat 0)
  let emptyMap = M.fromList $ zip (M.elems ref) (repeat [])
  countMaps <- mapM (fastq2countIO ref gNameZeroMap) fqinputs 
  let mergedMap = L.foldl' (M.unionWith (++)) emptyMap countMaps
  return mergedMap
  

-- one fastq file to all the gRNA names and their counts, if no count, zero will be provided. The numbers are in the form of [Int], because they need to be merged for multiple inputs.
fastq2countIO :: Ord k => M.Map B.ByteString k
     -> M.Map k Int -> FilePath -> IO (M.Map k [Int])
-- take a Map of reference and a fastq file
fastq2countIO refMap refMapZero fastqinput = do
  inputseq <- cleanSeq <$> inputIO fastqinput
  return (countReads inputseq refMap refMapZero)
  
-- IO
inputIO = B.readFile
--inputIO p = mmapFileByteString p Nothing

-- key line
cleanSeq::B.ByteString -> [B.ByteString]
cleanSeq = filter (\x->B.length x == 20) . map getgRNA .
           filter (not . B.null) . map trimSeq . getSeq

-- helpers
-- get sequence from fastq, through away name, quality and + lines
getSeq::B.ByteString -> [B.ByteString]
getSeq = every 4 . addline . addline . B.lines --add two empty line, to keep the first read sequence
  where every n xs = case drop (n-1) xs of
              (y:ys) -> y : every n ys
              [] -> []
        addline = (:) (B.pack "")

-- handle substrings for both forward sequences and reverse complementary 
trimSeq::B.ByteString -> B.ByteString
trimSeq xs = if Bs.isInfixOf (B.pack "ACACC") xs 
             then (fst . B.breakSubstring (B.pack "TTAGAGC") . snd . B.breakSubstring (B.pack "ACACC")) xs
             else (snd . B.breakSubstring (B.pack "GCTCTAA") . fst . B.breakSubstring (B.pack "GGTGT")) xs

--only take 19nt, because in the reference list it doens't contain first G; sam library is 20 nt for guides
--TODO:this part needs to be more flexible
getgRNA::B.ByteString -> B.ByteString
getgRNA xs = case B.head xs of
              'A' -> (B.take 20 . B.drop 5) xs
              'G' -> (B.take 20 . reverseComp) xs

reverseComp::B.ByteString -> B.ByteString
reverseComp x = B.reverse . comp $ x
  where comp = B.map comp'
        comp' x 
          |x == 'A'='T'
          |x == 'T'='A'
          |x == 'G'='C'
          |x == 'C'='G'
          |otherwise = 'N'

getLib :: B.ByteString -> [(B.ByteString, B.ByteString)]
getLib = tail . map (toPairs . B.split '\t') . B.lines

toPairs x = (head x, last x)

countReads input ref refZero = mergeCount . M.fromList .
                       groupCount . 
                       map Maybe.fromJust . filter (/= Nothing) . map lookup $
                       input
                    where lookup seq = M.lookup seq ref 
                     -- get gRNA name, throw away sequence
                          mergeCount = M.map (\x -> [x]) . M.unionWith (+) refZero
                     -- combine with a zero count map, which include all the gRNA names from reference

