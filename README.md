# crisprScreen

This is a small program to generate read count matrix for CRISPR screening experiments, which can be used for downstream analysis such as [MAGeCK](http://liulab.dfci.harvard.edu/Mageck/) from _Liu_ lab. 

Please feel free to visit [Martin Lab](https://www.bcm.edu/people/view/james-martin-m-d-ph-d/b18a4d39-ffed-11e2-be68-080027880ca6) at Baylor College of Medicine to see our research. 

## installation

_crisprScreen_ is written in Haskell using stack for package management. It can be easily installed in different systems.

### prerequisite

Please install stack to your system.

The instruction can be found in [here](https://github.com/commercialhaskell/stack/blob/master/doc/GUIDE.md) and [here](http://docs.haskellstack.org/en/stable/README/).

### pull most updated version of crisprScreen from our Bitbucket repository

```bash
git clone https://mz1@bitbucket.org/mz1/crisprscreen.git
cd crisprscreen
```
### install crisprScreen through stack

```bash
stack setup && stack build
```

### test crisprScreen installation
```bash
stack exec crisprScreen
```

## run crisprScreen

Call the program from _crisprScreen_ folder:

```bash
stack exec crisprScreen <input fastq file directory> <reference file>
```

The input fastq directory contains all raw fastq files from sequencing. The reference file is a tab-delimited text file contains gRNA sequences and names of the screening library (in the format of: _geneName_-_gRNACount_). For example:

```bash
GCCTTCTCAATACCCATTCG	Zglp1-1
GCTTCTCAATACCCATTCGC	Zglp1-2
GGACCTGGGTGGCCGGCGAA	Zglp1-3
GACCTGGGTGGCCGGCGAAG	Zglp1-4
GCGCGCCACGCACCTGATAT	Zglp1-5
GATGCGAACCCAGTGCGCTA	Mdn1-1
GCTCTGGTTTTGCTTGATCG	Mdn1-2
GTCTGTAAATCGGTTTCGTA	Mdn1-3
GAAGGTAGAGCGTACTGTCA	Mdn1-4
GTGCTGGGTCGATGAACCAG	Mdn1-5
```

We used mouse loss-of-function library from _Kosuke Yusa_ lab (Koike-Yusa, Li _et al_, Nat Biotechnol 2014), this reference file is included in the package under _ref_ folder.

By default, the column names are fastq file names. The output matrix will look like this: 

```bash
sgRNA	Gene	sample1	sample2	sample3	sample4
0610007L01Rik-1	0610007L01Rik	2	8	4	0
0610007L01Rik-2	0610007L01Rik	7	5	9	8
0610007L01Rik-3	0610007L01Rik	5	3	6	0
0610007L01Rik-4	0610007L01Rik	41	8	12	44
0610007L01Rik-5	0610007L01Rik	44	29	42	54
0610007P14Rik-1	0610007P14Rik	16	9	21	3
0610007P14Rik-2	0610007P14Rik	0	0	0	0
0610007P14Rik-3	0610007P14Rik	1	5	0	0
0610007P14Rik-4	0610007P14Rik	3	4	0	0
```

Questions, comments and feedbacks are always welcome. 

Min Zhang (mz1 at bcm dot edu)

